/**
 * Copyright 2013 Kandy Software Inc.,
 */
package aqua.semantics;

import aqua.type.Type;

/**
 * A {@link LiteralSymbol} represents a literal.
 */
public class LiteralSymbol extends Symbol {

    private String value;

    /**
     * Literal's don't have names. You can't have literal with an unknown type.
     */
    private LiteralSymbol() {
        super(null, null);
    }

    /**
     * Literal's don't have names. If type is known, use this constructor.
     *
     * @param type  type
     * @param value literal
     */
    public LiteralSymbol(Type type, String value) {
        super(null, type);
        this.value = value;
    }

    @Override
    public String toString() {
        return String.format("%s(%s)", type, value);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        LiteralSymbol other = (LiteralSymbol) obj;
        if (type == null) {
            if (other.type != null)
                return false;
        } else if (!type.equals(other.type))
            return false;
        return true;
    }
}

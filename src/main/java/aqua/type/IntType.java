/**
 * Copyright 2013 Kandy Software Inc.,
 */
package aqua.type;

/**
 * int
 */
public class IntType extends NumericType {

    public IntType() {
        super(Type.Kind.INT);
        this.sizeInBits = 32;
    }

    public IntType(int sizeInBits) {
        this();
        this.sizeInBits = sizeInBits;
    }

    @Override
    public String llvmType() {
        return String.format("i%s", sizeInBits);
    }

    @Override
    public String llvmPointerType() {
        return String.format("i%s*", sizeInBits);
    }

    @Override
    public String llvmAlignment() {
        return String.format("align 4");
    }

    @Override
    public String toString() {
        return "int";
    }

    /**
     * @see aqua.type.Type#clone()
     */
    @Override
    public Type clone() {
        return new IntType(this.sizeInBits);
    }

}

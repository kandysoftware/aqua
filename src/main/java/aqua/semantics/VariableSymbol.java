/**
 * Copyright 2013 Kandy Software Inc.,
 */
package aqua.semantics;

import aqua.type.Type;

/**
 * A {@link VariableSymbol} represents an identifier
 */
public class VariableSymbol extends Symbol {

	public VariableSymbol() {
		super(null, null);
	}

	public VariableSymbol(String name) {
		super(name, null);
	}

	public VariableSymbol(String name, Type type) {
		super(name, type);
	}

	@Override
	public String toString() {
		return "Variable [name=" + name + ", type=" + type + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VariableSymbol other = (VariableSymbol) obj;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}
}

/**
 * Copyright 2013 Kandy Software Inc.,
 */
package aqua.ir.llvm;

import aqua.grammar.AquaBaseListener;
import aqua.grammar.AquaParser;
import aqua.semantics.*;
import aqua.type.*;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeProperty;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

/**
 * Generates bitcode for a given symbol.
 */
public class BitcodeGenerator extends AquaBaseListener {

    public static int lambdaCount = 0;

    private LLVMStack llvmStack;

    protected PrintWriter writer;

    private GlobalScope globalScope;

    private ParseTreeProperty<Symbol> ast;

    protected ParseTreeProperty<String> values = new ParseTreeProperty<>();

    public BitcodeGenerator(PrintWriter writer, GlobalScope globalScope, ParseTreeProperty<Symbol> ast) {
        this.writer = writer;
        this.globalScope = globalScope;
        this.ast = ast;
    }

    private String getValue(ParseTree ctx) {
        return values.get(ctx);
    }

    private void setValue(ParseTree ctx, String s) {
        values.put(ctx, s);
    }

    private PrintWriter printf(String format, Object... args) {
        return writer.printf(format, args);
    }

    private void println(String x) {
        writer.println(x);
    }

    @Override
    public void enterCompilationUnit(@NotNull AquaParser.CompilationUnitContext ctx) {
        println("target triple = \"x86_64-pc-linux-gnu\"\n");
        println("declare i32 @printf(i8*, ...) #1");
        println("@.str = private unnamed_addr constant [3 x i8] c\"%d\\00\", align 1\n");
        println("define i32 @main() #0 {");
    }

    @Override
    public void exitCompilationUnit(@NotNull AquaParser.CompilationUnitContext ctx) {
        // String result = createTempIdentifier();
        // String printfexit = createTempIdentifier();
        // printf("%s = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([3 x i8]* @.str, i32 0, i32 0), i32 %s)\n", printfexit, result);

        println("ret i32 0");
        println("}");
    }

    @Override
    public void exitMutable(@NotNull AquaParser.MutableContext ctx) {
        Symbol symbol = ast.get(ctx);
        System.out.println(symbol);
    }

    @Override
    public void enterFunction(@NotNull AquaParser.FunctionContext ctx) {
        llvmStack = new LLVMStack();
        FunctionSymbol functionSymbol = (FunctionSymbol) ast.get(ctx);

        println("");
        printf("define %s @%s", functionSymbol.getType().llvmType(), functionSymbol.getName());
        printf("(");
        for (Iterator<Symbol> iter = functionSymbol.getParameters().getMembers().iterator(); iter.hasNext(); ) {
            Symbol param = iter.next();
            printf("%s %s", param.getType().llvmType(), param.llvmLocal());
            if (iter.hasNext()) printf(", ");
        }
        println(") #0 {");
    }

    @Override
    public void exitFunction(@NotNull AquaParser.FunctionContext ctx) {
        String result = getValue(ctx.functionExpression().blockExpression());
        printf("ret i32 %s", result);
        println("}");
    }

    @Override
    public void exitFormalParameter(@NotNull AquaParser.FormalParameterContext ctx) {
        Symbol param = ast.get(ctx);
        String var = llvmStack.nextTempVariable();
        Type type = param.getType();

        printf("%s = alloca %s, %s\n", var, type.llvmType(), type.llvmAlignment());
        printf("store %s %s, %s %s, %s\n", type.llvmType(), param.llvmLocal(), type.llvmPointerType(), var, type.llvmAlignment());

        String load = llvmStack.nextTempVariable();
        printf("%s = load %s %s, %s\n", load, type.llvmPointerType(), var, type.llvmAlignment());

        param.setAddress(load);
        setValue(ctx, load);
    }


    @Override
    public void enterNonemptyBlock(@NotNull AquaParser.NonemptyBlockContext ctx) {
    }

    /**
     * Returns value of last expression in the block
     *
     * @param ctx { ... last }
     */
    @Override
    public void exitNonemptyBlock(@NotNull AquaParser.NonemptyBlockContext ctx) {
        String result = getValue(ctx.getChild(ctx.getChildCount() - 2));
        setValue(ctx, result);
    }


    @Override
    public void exitCall(@NotNull AquaParser.CallContext ctx) {
    }

    /**
     * Returns an i32 pointer to result
     *
     * @param ctx (MUL | DIV | MOD)
     */
    @Override
    public void exitMultiplicative(@NotNull AquaParser.MultiplicativeContext ctx) {
        String left = getValue(ctx.expression(0));
        String right = getValue(ctx.expression(1));
        String loadLeft = llvmStack.nextTempVariable();
        String loadRight = llvmStack.nextTempVariable();
        printf("%s = load i32* %s, align 4\n", loadLeft, left);
        printf("%s = load i32* %s, align 4\n", loadRight, right);

        String temp = llvmStack.nextTempVariable();
        if (ctx.op.getType() == AquaParser.MUL) {
            printf("%s = mul nsw i32 %s, %s\n", temp, loadLeft, loadRight);
        } else if (ctx.op.getType() == AquaParser.DIV) {
            printf("%s = sdiv i32 %s, %s\n", temp, loadLeft, loadRight);
        } else if (ctx.op.getType() == AquaParser.MOD) {
            printf("%s = srem i32 %s, %s\n", temp, loadLeft, loadRight);
        }
        setValue(ctx, temp);
    }

    /**
     * Returns an i32 pointer to result
     *
     * @param ctx (ADD | SUB)
     */
    @Override
    public void exitAdditive(@NotNull AquaParser.AdditiveContext ctx) {
        ctx.expression(0);
        String left = getValue(ctx.expression(0));
        String right = getValue(ctx.expression(1));
        String loadLeft = llvmStack.nextTempVariable();
        String loadRight = llvmStack.nextTempVariable();
        printf("%s = load i32* %s, align 4\n", loadLeft, left);
        printf("%s = load i32* %s, align 4\n", loadRight, right);

        String temp = llvmStack.nextTempVariable();
        String op = ctx.op.getType() == AquaParser.ADD ? "add" : "sub";
        printf("%s = %s nsw i32 %s, %s\n", temp, op, loadLeft, loadRight);
        setValue(ctx, temp);
    }

    /**
     * Always yeilds a i1 value
     *
     * @param ctx (LE | GE | GT | LT)
     */
    @Override
    public void exitRelational(@NotNull AquaParser.RelationalContext ctx) {
        String left = getValue(ctx.expression(0));
        String right = getValue(ctx.expression(1));
        NumericType leftType = (NumericType) ast.get(ctx.expression(0)).getType();
        NumericType rightType = (NumericType) ast.get(ctx.expression(1)).getType();

        String loadLeft = llvmStack.nextTempVariable();
        String loadRight = llvmStack.nextTempVariable();
        printf("%s = load %s %s, align 4\n", loadLeft, leftType.llvmPointerType(), left);
        printf("%s = load %s %s, align 4\n", loadRight, rightType.llvmPointerType(), right);

        Type widest = TypeUnifier.widest(leftType, rightType);
        String cmp = widest instanceof IntType ? "icmp" : "fcmp";
        String ty = widest.llvmType();

        String result = llvmStack.nextTempVariable();
        String cond = "";
        if (ctx.op.getType() == AquaParser.LE) {
            cond = "sle";
        } else if (ctx.op.getType() == AquaParser.GE) {
            cond = "sge";
        } else if (ctx.op.getType() == AquaParser.GT) {
            cond = "sgt";
        } else if (ctx.op.getType() == AquaParser.LT) {
            cond = "slt";
        }
        printf("%s = %s %s %s %s, %s\n", result, cmp, cond, ty, loadLeft, loadRight);
        setValue(ctx, result);
    }

    /**
     * Always yeilds a i1 value
     *
     * @param ctx (EQUAL | NOTEQUAL)
     */
    @Override
    public void exitComparision(@NotNull AquaParser.ComparisionContext ctx) {
        String left = getValue(ctx.expression(0));
        String right = getValue(ctx.expression(1));
        String loadLeft = llvmStack.nextTempVariable();
        String loadRight = llvmStack.nextTempVariable();
        printf("%s = load i32* %s, align 4\n", loadLeft, left);
        printf("%s = load i32* %s, align 4\n", loadRight, right);

        String temp = llvmStack.nextTempVariable();
        if (ctx.op.getType() == AquaParser.EQUAL) {
            printf("%s = icmp eq i32 %s, %s\n", temp, loadLeft, loadRight);
        } else if (ctx.op.getType() == AquaParser.NOTEQUAL) {
            printf("%s = icmp ne i32 %s, %s\n", temp, loadLeft, loadRight);
        }
        setValue(ctx, temp);
    }

    @Override
    public void exitAssignment(@NotNull AquaParser.AssignmentContext ctx) {
        String left = getValue(ctx.expression(0));
        String right = getValue(ctx.expression(1));
        printf("store i32 %s, i32* %s, align 4\n", right, left);
    }

    @Override
    public void exitLiteral(@NotNull AquaParser.LiteralContext ctx) {
        setValue(ctx, getValue(ctx.literalExpression()));
    }

    @Override
    public void exitInteger(@NotNull AquaParser.IntegerContext ctx) {
        String temp = llvmStack.nextTempVariable();
        printf("%s = alloca i32, align 4\n", temp);
        printf("store i32 %s, i32* %s, align 4\n", ctx.IntegerLiteral().getText(), temp);
        setValue(ctx, temp);
    }

    @Override
    public void enterBoolean(@NotNull AquaParser.BooleanContext ctx) {
        Boolean value = Boolean.valueOf(ctx.BooleanLiteral().getText());
        String temp = llvmStack.nextTempVariable();
        printf("%s = alloca i1, align 4\n", temp);
        printf("store i1 %s, i1* %s, align 4\n", value ? 1 : 0, temp);
        setValue(ctx, temp);
    }

    @Override
    public void exitId(@NotNull AquaParser.IdContext ctx) {
//        String id = createLocalIdentifier(ctx.Identifier().getText());
//        printf("%s = alloca i32, align 4\n", id);
//        setValue(ctx, id);
    }
}

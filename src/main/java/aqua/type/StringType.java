/**
 * Copyright 2013 Kandy Software Inc.,
 */
package aqua.type;

/**
 * String type.
 */
public class StringType extends Type {

	public StringType() {
		super(Kind.STRING);
	}

	@Override
	public String toString() {
		return "string";
	}

	/**
	 * @see aqua.type.Type#clone()
	 */
	@Override
	public Type clone() {
		return new StringType();
	}
}

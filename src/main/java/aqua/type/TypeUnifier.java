package aqua.type;

/**
 * Type inferencer and unifier.
 */
public class TypeUnifier {
    /**
     * Unifies two given types.
     *
     * @param lhs left
     * @param rhs right
     * @return unified type
     */
    public static Type unifyTypes(Type lhs, Type rhs) {
        if (lhs == null && rhs == null) return new UnknownType();
        if (lhs == null && rhs != null) return rhs;
        if (lhs instanceof UnknownType && !(rhs instanceof UnknownType)) return rhs;
        if (isNumeric(lhs) && isNumeric(rhs)) return widest(lhs, rhs);
        return new UnknownType();
    }

    /**
     * @param type a type
     * @return true if type is Int|Float
     */
    public static boolean isNumeric(Type type) {
        return type != null && (type.isOfKind(Type.Kind.INT) || type.isOfKind(Type.Kind.FLOAT));
    }


    /**
     * Returns widest of the two.
     *
     * @param type1 type
     * @param type2 type
     * @return one of the given two or Unknown
     */
    public static Type widest(Type type1, Type type2) {
        if (type1 == null || type2 == null) return new UnknownType();
        if (type1.isOfKind(Type.Kind.FLOAT) || type2.isOfKind(Type.Kind.FLOAT)) new FloatType();
        return new IntType();
    }
}

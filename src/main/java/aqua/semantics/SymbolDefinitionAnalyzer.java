/**
 * Copyright 2013 Kandy Software Inc.,
 */
package aqua.semantics;

import aqua.grammar.AquaBaseListener;
import aqua.grammar.AquaParser;
import aqua.type.*;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeProperty;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Map;

/**
 * A {@link SymbolDefinitionAnalyzer} builds a symbol table of an input program.
 */
public class SymbolDefinitionAnalyzer extends AquaBaseListener {

    private static final Logger log = LogManager.getLogger(SymbolDefinitionAnalyzer.class);

    private Scope currentScope;

    private GlobalScope globalScope;

    private ParseTreeProperty<Symbol> ast;

    public SymbolDefinitionAnalyzer(GlobalScope globalScope, ParseTreeProperty<Symbol> ast) {
        this.globalScope = globalScope;
        this.ast = ast;
    }

    /**
     * Save AST symbol for CST.
     *
     * @param ctx    node
     * @param symbol symbol
     */
    private void putSymbol(ParserRuleContext ctx, Symbol symbol) {
        ast.put(ctx, symbol);
    }

    /**
     * Get symbol associated with given node.
     *
     * @param node node
     * @return symbol associated with Node
     */
    private Symbol getSymbol(ParseTree node) {
        return ast.get(node);
    }

    @Override
    public void enterCompilationUnit(@NotNull AquaParser.CompilationUnitContext ctx) {
        currentScope = new LocalScope(globalScope);
    }

    @Override
    public void exitCompilationUnit(@NotNull AquaParser.CompilationUnitContext ctx) {
    }

    @Override
    public void exitMutable(@NotNull AquaParser.MutableContext ctx) {
        String name = ctx.Identifier().getText();
        Type type = TypeResolver.resolveType(ctx.typeAnnotation());
        Symbol rhs = getSymbol(ctx.expression());
        rhs.setName(name);

        if (rhs instanceof FunctionSymbol) {
            currentScope.define(rhs);
        } else {
            DeclarationSymbol decl = new DeclarationSymbol(name, type, rhs);
            putSymbol(ctx, decl);
            currentScope.define(decl);
        }
    }

    @Override
    public void exitId(@NotNull AquaParser.IdContext ctx) {
        String name = ctx.Identifier().getText();
        VariableSymbol var = new VariableSymbol(name, new UnknownType());
        putSymbol(ctx, var);
        currentScope.define(var);
    }

    @Override
    public void exitFunction(@NotNull AquaParser.FunctionContext ctx) {
        putSymbol(ctx, getSymbol(ctx.functionExpression()));
    }

    @Override
    public void enterFunctionExpression(@NotNull AquaParser.FunctionExpressionContext ctx) {
        LocalScope functionScope = new LocalScope(currentScope);
        String name = "";
        FunctionSymbol function = new FunctionSymbol(functionScope, name, new UnknownType());
        putSymbol(ctx, function);
        currentScope = functionScope;
    }

    @Override
    public void exitFunctionExpression(@NotNull AquaParser.FunctionExpressionContext ctx) {
        FunctionSymbol function = (FunctionSymbol) getSymbol(ctx);
        TupleSymbol parameters = (TupleSymbol) getSymbol(ctx.formalParameters());
        function.setParameters(parameters);
        currentScope = currentScope.getEnclosingScope();
    }

    /**
     * Result is a TupleSymbol
     *
     * @param ctx (param:type, param:type ...
     */
    @Override
    public void enterFormalParameters(@NotNull AquaParser.FormalParametersContext ctx) {
        LocalScope parametersScope = new LocalScope(currentScope);
        TupleSymbol tuple = new TupleSymbol(parametersScope);
        putSymbol(ctx, tuple);
        currentScope = parametersScope;
    }

    /**
     * Result is a TupleSymbol
     *
     * @param ctx param:type)
     */
    @Override
    public void exitFormalParameters(@NotNull AquaParser.FormalParametersContext ctx) {
        Map<String, Symbol> parameters = currentScope.getSymbolTable();
        TupleSymbol tuple = (TupleSymbol) getSymbol(ctx);
        tuple.setMembers(new ArrayList<>(parameters.values()));
        putSymbol(ctx, tuple);
        currentScope = currentScope.getEnclosingScope();
    }

    @Override
    public void exitFormalParameter(@NotNull AquaParser.FormalParameterContext ctx) {
        String name = ctx.Identifier().getText();
        Type type = TypeResolver.resolveType(ctx.typeAnnotation());
        VariableSymbol var = new VariableSymbol(name, type);
        putSymbol(ctx, var);
        currentScope.define(var);
    }

    @Override
    public void enterNonemptyBlock(@NotNull AquaParser.NonemptyBlockContext ctx) {
        LocalScope blockScope = new LocalScope(currentScope);
        BlockSymbol block = new BlockSymbol(blockScope, new UnknownType());
        putSymbol(ctx, block);
        currentScope = blockScope;
    }

    @Override
    public void exitNonemptyBlock(@NotNull AquaParser.NonemptyBlockContext ctx) {
        currentScope = currentScope.getEnclosingScope();
    }

    /**
     * Result is a CallSymbol
     *
     * @param ctx id (
     */
    @Override
    public void exitCall(@NotNull AquaParser.CallContext ctx) {
        String name = ctx.Identifier().getText();
        TupleSymbol arguments = (TupleSymbol) getSymbol(ctx.arguments());
        CallSymbol call = new CallSymbol(currentScope, name, arguments, new UnknownType());
        putSymbol(ctx, call);
    }

    /**
     * Result is a TupleSymbol
     *
     * @param ctx (argument*)
     */
    @Override
    public void enterArguments(@NotNull AquaParser.ArgumentsContext ctx) {
        LocalScope tupleScope = new LocalScope(currentScope);
        TupleSymbol tuple = new TupleSymbol(tupleScope);
        putSymbol(ctx, tuple);
        currentScope = tupleScope;
    }

    /**
     * Result is a TupleSymbol
     *
     * @param ctx (argument*)
     */
    @Override
    public void exitArguments(@NotNull AquaParser.ArgumentsContext ctx) {
        currentScope = currentScope.getEnclosingScope();
    }

    /**
     * Expression is a Symbol
     *
     * @param ctx expression
     */
    @Override
    public void exitArgument(@NotNull AquaParser.ArgumentContext ctx) {
        Symbol arg = getSymbol(ctx.expression());
        putSymbol(ctx, arg);
        currentScope.define(arg);
    }

    /**
     * Result is TupleSymbol.
     *
     * @param ctx (exp,...
     */
    @Override
    public void enterTuple(@NotNull AquaParser.TupleContext ctx) {
        LocalScope tupleScope = new LocalScope(currentScope);
        TupleSymbol tuple = new TupleSymbol(tupleScope);
        currentScope.define(tuple);
        currentScope = tupleScope;
    }

    /**
     * Result is TupleSymbol.
     *
     * @param ctx ...)
     */
    @Override
    public void exitTuple(@NotNull AquaParser.TupleContext ctx) {
        currentScope = currentScope.getEnclosingScope();
    }

    /**
     * Result is a BinaryExpressionSymbol
     *
     * @param ctx (MUL | DIV | MOD)
     */
    @Override
    public void exitMultiplicative(@NotNull AquaParser.MultiplicativeContext ctx) {
        Symbol left = getSymbol(ctx.expression(0));
        Symbol right = getSymbol(ctx.expression(1));

        String operator = AquaParser.tokenNames[ctx.op.getType()];
        BinaryExpressionSymbol multiplicative = new BinaryExpressionSymbol(left, operator, right);
        putSymbol(ctx, multiplicative);
    }

    /**
     * Result is a BinaryExpressionSymbol
     *
     * @param ctx (ADD | SUB)
     */
    @Override
    public void exitAdditive(@NotNull AquaParser.AdditiveContext ctx) {
        Symbol left = getSymbol(ctx.expression(0));
        Symbol right = getSymbol(ctx.expression(1));

        String operator = AquaParser.tokenNames[ctx.op.getType()];
        BinaryExpressionSymbol additive = new BinaryExpressionSymbol(left, operator, right);
        putSymbol(ctx, additive);
    }

    /**
     * Result is a BinaryExpressionSymbol
     *
     * @param ctx (LE | GE | GT | LT)
     */
    @Override
    public void exitRelational(@NotNull AquaParser.RelationalContext ctx) {
        Symbol left = getSymbol(ctx.expression(0));
        Symbol right = getSymbol(ctx.expression(1));

        String operator = AquaParser.tokenNames[ctx.op.getType()];
        BinaryExpressionSymbol relational = new BinaryExpressionSymbol(left, operator, right);
        relational.setType(new BoolType());
        putSymbol(ctx, relational);
    }

    // literals

    @Override
    public void exitLiteral(@NotNull AquaParser.LiteralContext ctx) {
        putSymbol(ctx, getSymbol(ctx.literalExpression()));
    }

    @Override
    public void exitInteger(@NotNull AquaParser.IntegerContext ctx) {
        String value = ctx.IntegerLiteral().getText();
        LiteralSymbol literal = new LiteralSymbol(new IntType(), value);
        putSymbol(ctx, literal);
    }

    @Override
    public void enterBoolean(@NotNull AquaParser.BooleanContext ctx) {
        String value = ctx.getText();
        LiteralSymbol literal = new LiteralSymbol(new BoolType(), value);
        putSymbol(ctx, literal);
    }
}

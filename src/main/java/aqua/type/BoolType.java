/**
 * Copyright 2013 Kandy Software Inc.,
 */
package aqua.type;

/**
 * bool
 */
public class BoolType extends NumericType {

    public BoolType() {
        super(Kind.BOOL);
        this.sizeInBits = 1;
    }

    @Override
    public String llvmType() {
        return String.format("i%s", sizeInBits);
    }

    @Override
    public String llvmPointerType() {
        return String.format("i%s*", sizeInBits);
    }

    @Override
    public String llvmAlignment() {
        return "";
    }

    /**
     * @see aqua.type.Type#clone()
     */
    @Override
    public Type clone() {
        return new BoolType();
    }

    @Override
    public String toString() {
        return "bool";
    }
}

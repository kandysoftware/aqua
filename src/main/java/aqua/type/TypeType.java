/**
 * Copyright 2013 Kandy Software Inc.,
 */
package aqua.type;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * User defined type declaration.
 */
public class TypeType extends Type {

	private List<Type> memberTypes;

	public TypeType() {
		super(Kind.TYPE);
		memberTypes = new ArrayList<>();
	}

	public TypeType(final List<Type> members) {
		super(Kind.TYPE);
		memberTypes = new ArrayList<>(members);
	}

	/**
	 * @see aqua.type.Type#clone()
	 */
	@Override
	public Type clone() {
		return new TypeType(memberTypes);
	}

	@Override
	public String toString() {
		return Arrays.toString(memberTypes.toArray(new Type[memberTypes.size()]));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((memberTypes == null) ? 0 : memberTypes.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TypeType other = (TypeType) obj;

		if (memberTypes.size() == other.memberTypes.size()) {
			int i = 0;
			for (final Type arg : memberTypes) {
				if (!other.memberTypes.contains(arg)) {
					return false;
				}
			}
			return true;
		}
		return false;
	}
}

package aqua.ir.llvm;

/**
 * Generates variables in sequence.
 */
public class LLVMStack {

    protected int sequence;

    public LLVMStack() {
    }

    public String nextTempVariable() {
        sequence++;
        return "%" + sequence;
    }
}

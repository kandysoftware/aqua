package aqua.semantics;

/**
 * Represents global scope.
 */
public class GlobalScope extends AbstractScope {

    public GlobalScope() {
        super(null);
    }

    @Override
    public String getScopeName() {
        return "global";
    }
}

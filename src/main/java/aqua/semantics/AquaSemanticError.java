/**
 * Copyright 2013 Kandy Software Inc.,
 */
package aqua.semantics;

import aqua.compiler.AquaMessage;

/**
 * A {@link AquaSemanticError} represents a compile time semantic error.
 */
public class AquaSemanticError extends AquaMessage {

	public AquaSemanticError(int line, int position, String description) {
		this.line = line;
		this.position = position;
		this.description = description;
	}
}

## The Aqua Language

Aqua is a high-level, general purpose, expression-oriented language.

## Resources

- **Source code:** <https://bitbucket.org/kandysoftware/aqua>
- **Git clone URL:** <git@bitbucket.org:kandysoftware/aqua.git>

## Currently Supported Platforms

- **GNU/Linux**
- **FreeBSD**
- **Windows**

## Download and Compile Source

First, acquire the source code by cloning the git repository:

    git clone git@bitbucket.org:kandysoftware/aqua.git

(If you are behind a firewall, you may need to use the `https` protocol instead of the `git` protocol:

    git config --global url."https://".insteadOf git://

Be sure to also configure your system to use the appropriate proxy settings, e.g. by setting the `https_proxy` and `http_proxy` variables.)


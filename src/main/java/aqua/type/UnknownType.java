/**
 * Copyright 2013 Kandy Software Inc.,
 */
package aqua.type;

/**
 * unknown
 */
public class UnknownType extends Type {

	public UnknownType() {
		super(Kind.UNKNOWN);
	}

	/**
	 * @see aqua.type.Type#clone()
	 */
	@Override
	public Type clone() {
		return new UnknownType();
	}

	@Override
	public String toString() {
		return "unknown";
	}

}

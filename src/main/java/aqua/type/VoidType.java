/**
 * Copyright 2013 Kandy Software Inc.,
 */
package aqua.type;

/**
 * void
 */
public class VoidType extends Type {

	public VoidType() {
		super(Kind.VOID);
	}

	/**
	 * @see aqua.type.Type#clone()
	 */
	@Override
	public Type clone() {
		return new VoidType();
	}

	@Override
	public String toString() {
		return "void";
	}
}

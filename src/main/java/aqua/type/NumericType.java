/**
 * Copyright 2013 Kandy Software Inc.,
 */
package aqua.type;

/**
 * int
 */
public abstract class NumericType extends Type {

    protected int sizeInBits;

    public NumericType(Kind k) {
        super(k);
    }

    public int getSizeInBits() {
        return sizeInBits;
    }

    public void setSizeInBits(int sizeInBits) {
        this.sizeInBits = sizeInBits;
    }

    @Override
    public String toString() {
        return "int";
    }
}

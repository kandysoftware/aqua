/**
 * Copyright 2013 Kandy Software Inc.,
 */
package aqua.semantics;

import java.util.ArrayList;
import java.util.List;

import aqua.type.Type;

/**
 * A {@link Symbol} represents a code artifact.
 */
public abstract class Symbol {

    protected String name;

    protected Type type;

    protected Scope scope;

    protected String address;

    /**
     * Symbol must have a name anda type;
     *
     * @param name this symbol's name
     */
    public Symbol(String name, Type type) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Scope getScope() {
        return scope;
    }

    public void setScope(Scope scope) {
        this.scope = scope;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String llvmLocal() {
        return "%" + name;
    }

    public String llvmGlobal() {
        return "@" + name;
    }

    public static List<Type> extractTypes(List<? extends Symbol> symbols) {
        List<Type> types = new ArrayList<>();
        for (Symbol symbol : symbols) {
            types.add(symbol.getType());
        }
        return types;
    }
}

/**
 * Copyright 2013 Kandy Software Inc.,
 */
package aqua.type;

/**
 * ⊥
 */
public class BottomType extends Type {

	public BottomType() {
		super(Kind.BOTTOM);
	}

	@Override
	public Boolean isOfKind(final Kind comp) {
		return true;
	}

	/**
	 * @see aqua.type.Type#clone()
	 */
	@Override
	public Type clone() {
		return new BottomType();
	}

	@Override
	public String toString() {
		return "⊥";
	}
}

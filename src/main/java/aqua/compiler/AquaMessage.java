/**
 * Copyright 2013 Kandy Software Inc.,
 */
package aqua.compiler;

/**
 * Base class for errors and warnings.
 */
public abstract class AquaMessage {

	protected int line;

	protected int position;

	protected String description;

	public AquaMessage() {
	}

	public AquaMessage(int line, int position, String description) {
		this.line = line;
		this.position = position;
		this.description = description;
	}

	public int getLine() {
		return line;
	}

	public void setLine(int line) {
		this.line = line;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}

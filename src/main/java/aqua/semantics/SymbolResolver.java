/**
 * Copyright 2013 Kandy Software Inc.,
 */
package aqua.semantics;

import aqua.grammar.AquaBaseListener;
import aqua.grammar.AquaParser;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeProperty;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * A {@link aqua.semantics.SymbolResolver} resolves forward and backward references.
 */
public class SymbolResolver extends AquaBaseListener {

    private static final Logger log = LogManager.getLogger(SymbolResolver.class);

    private Scope currentScope;

    private GlobalScope globalScope;

    private ParseTreeProperty<Symbol> ast;

    private List<String> errors;

    public SymbolResolver(GlobalScope globalScope, ParseTreeProperty<Symbol> ast) {
        this.globalScope = globalScope;
        this.ast = ast;
        this.errors = new ArrayList<>(128);
    }

    private void error(Token t, String msg) {
        this.errors.add(String.format("line %d:%d %s", t.getLine(), t.getCharPositionInLine(), msg));
    }

    @Override
    public void enterCompilationUnit(@NotNull AquaParser.CompilationUnitContext ctx) {
        currentScope = globalScope;
    }

    @Override
    public void exitCompilationUnit(@NotNull AquaParser.CompilationUnitContext ctx) {
        if (!this.errors.isEmpty()) {
            for (String error : this.errors) {
                System.out.println(error);
            }
            System.exit(0);
        }
    }

    @Override
    public void enterFunction(@NotNull AquaParser.FunctionContext ctx) {
        currentScope = ast.get(ctx).getScope();
    }

    @Override
    public void exitFunction(@NotNull AquaParser.FunctionContext ctx) {
        currentScope = currentScope.getEnclosingScope();
    }

    @Override
    public void enterNonemptyBlock(@NotNull AquaParser.NonemptyBlockContext ctx) {
        currentScope = ast.get(ctx).getScope();
    }

    @Override
    public void exitNonemptyBlock(@NotNull AquaParser.NonemptyBlockContext ctx) {
        currentScope = currentScope.getEnclosingScope();
    }

    /**
     * Resolve CallSymbol
     *
     * @param ctx id(arg*)
     */
    @Override
    public void exitCall(@NotNull AquaParser.CallContext ctx) {
        String functionName = ctx.Identifier().getText();
        Symbol symbol = currentScope.resolve(functionName);

        if (symbol == null) {
            error(ctx.Identifier().getSymbol(), "function not defined: " + functionName);
        } else if (!(symbol instanceof FunctionSymbol)) {
            error(ctx.Identifier().getSymbol(), functionName + " is not a function");
        }
    }

    /**
     * Resolve TupleSymbol
     *
     * @param ctx (argument*)
     */
    @Override
    public void enterArguments(@NotNull AquaParser.ArgumentsContext ctx) {
        currentScope = ast.get(ctx).getScope();
    }

    /**
     * Resolve TupleSymbol
     *
     * @param ctx (argument*)
     */
    @Override
    public void exitArguments(@NotNull AquaParser.ArgumentsContext ctx) {
        currentScope = currentScope.getEnclosingScope();
    }

    @Override
    public void exitId(@NotNull AquaParser.IdContext ctx) {
        String name = ctx.Identifier().getSymbol().getText();
        Symbol var = currentScope.resolve(name);

        if (var == null) {
            error(ctx.Identifier().getSymbol(), "no such variable: " + name);
        }

        if (var instanceof FunctionSymbol) {
            error(ctx.Identifier().getSymbol(), name + " is not a variable");
        }
    }
}

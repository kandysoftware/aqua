/**
 * Copyright 2013 Kandy Software Inc.,
 */
package aqua.semantics;

import aqua.type.Type;

/**
 * A {@link FunctionSymbol} represents a function definition.
 */
public class FunctionSymbol extends Symbol {

    protected TupleSymbol parameters;

    public FunctionSymbol(Scope scope, String name, Type returnType) {
        super(name, returnType);
        this.scope = scope;
    }

    public FunctionSymbol(Scope scope, String name, TupleSymbol parameters, Type returnType) {
        super(name, returnType);
        this.scope = scope;
        this.parameters = parameters;
    }

    public TupleSymbol getParameters() {
        return parameters;
    }

    public void setParameters(TupleSymbol parameters) {
        this.parameters = parameters;
    }

    @Override
    public String toString() {
        return String.format("%s %s :%s", name, parameters, type);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FunctionSymbol that = (FunctionSymbol) o;

        if (parameters != null ? !parameters.equals(that.parameters) : that.parameters != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return parameters != null ? parameters.hashCode() : 0;
    }
}

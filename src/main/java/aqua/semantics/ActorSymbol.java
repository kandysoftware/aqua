/**
 * Copyright 2013 Kandy Software Inc.,
 */
package aqua.semantics;

import aqua.type.Type;

/**
 * A {@link ActorSymbol} represents an actor expression
 */
public class ActorSymbol extends Symbol {

	private Type returnType;

	private TupleSymbol tuple;

	public ActorSymbol(String name, TupleSymbol tuple, Type returnType) {
		super(name, tuple.getType());
		this.tuple = tuple;
		this.returnType = returnType;
	}

	public Type getReturnType() {
		return returnType;
	}

	public void setReturnType(Type returnType) {
		this.returnType = returnType;
	}

	public TupleSymbol getTuple() {
		return tuple;
	}

	public void setTuple(TupleSymbol tuple) {
		this.tuple = tuple;
	}

	@Override
	public String toString() {
		return "Actor [name=" + name + ", returnType=" + returnType + ", tuple=" + tuple + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((returnType == null) ? 0 : returnType.hashCode());
		result = prime * result + ((tuple == null) ? 0 : tuple.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ActorSymbol other = (ActorSymbol) obj;
		if (returnType == null) {
			if (other.returnType != null)
				return false;
		} else if (!returnType.equals(other.returnType))
			return false;
		if (tuple == null) {
			if (other.tuple != null)
				return false;
		} else if (!tuple.equals(other.tuple))
			return false;
		return true;
	}
}

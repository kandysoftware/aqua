package aqua.semantics;

import java.util.Map;

/**
 * Encapsulates blocks, tuples and functions
 */
public interface Scope {
    /**
     * @return scope name
     */
    public String getScopeName();

    /**
     * Where to look next for symbols
     */
    public Scope getEnclosingScope();

    /**
     * Define a symbol in the current scope
     */
    public void define(Symbol symbol);

    /**
     * Look up name in this scope or in enclosing scope if not here
     */
    public Symbol resolve(String name);

    /**
     * @return symbols in current scope
     */
    public Map<String, Symbol> getSymbolTable();
}

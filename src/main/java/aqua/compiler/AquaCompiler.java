/**
 * Copyright 2013 Kandy Software Inc.,
 */
package aqua.compiler;

import java.io.PrintWriter;

import aqua.grammar.AquaLexer;
import aqua.grammar.AquaParser;
import aqua.ir.llvm.BitcodeGenerator;
import aqua.semantics.*;
import aqua.type.TypeChecker;
import org.antlr.v4.runtime.ANTLRFileStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeProperty;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

/**
 * An AquaCompiler. Compiles aqua programs to LLVM bitcode.
 */
public class AquaCompiler {

    private static final Logger log = LogManager.getLogger(AquaCompiler.class);

    // TODO: Accept this as an argument
    private static String destination = "dist";

    /**
     * @param args source files
     */
    public static void main(String[] args) {
        PrintWriter writer = null;
        try {
            String aquaSource = args[0];
            writer = new PrintWriter("dist/bc.ll", "UTF-8");

            AquaLexer lexer = new AquaLexer(new ANTLRFileStream(aquaSource));
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            AquaParser parser = new AquaParser(tokens);

            parser.removeErrorListeners();
            parser.addErrorListener(new UnderlineListener());

            ParseTree tree = parser.compilationUnit();
            ParseTreeWalker walker = new ParseTreeWalker();
            ParseTreeProperty<Symbol> ast = new ParseTreeProperty<>();
            GlobalScope globalScope = new GlobalScope();

            walker.walk(new SymbolDefinitionAnalyzer(globalScope, ast), tree);
            walker.walk(new SymbolResolver(globalScope, ast), tree);
            walker.walk(new TypeChecker(globalScope, ast), tree);
            walker.walk(new BitcodeGenerator(writer, globalScope, ast), tree);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            writer.close();
        }
    }
}

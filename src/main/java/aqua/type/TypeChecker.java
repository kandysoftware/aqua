/**
 * Copyright 2013 Kandy Software Inc.,
 */
package aqua.type;

import aqua.grammar.AquaBaseListener;
import aqua.grammar.AquaParser;
import aqua.semantics.*;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeProperty;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static aqua.type.TypeUnifier.unifyTypes;

/**
 * Checks to make sure promised types match declared types.
 */
public class TypeChecker extends AquaBaseListener {

    private static final Logger log = LogManager.getLogger(TypeChecker.class);

    private Scope currentScope;

    private GlobalScope globalScope;

    private ParseTreeProperty<Symbol> ast;

    public TypeChecker(GlobalScope globalScope, ParseTreeProperty<Symbol> ast) {
        this.globalScope = globalScope;
        this.ast = ast;
    }

    @Override
    public void enterCompilationUnit(@NotNull AquaParser.CompilationUnitContext ctx) {
        currentScope = globalScope;
    }

    @Override
    public void exitCompilationUnit(@NotNull AquaParser.CompilationUnitContext ctx) {
        super.exitCompilationUnit(ctx);
    }

    @Override
    public void exitMutable(@NotNull AquaParser.MutableContext ctx) {
        Symbol rhs = ast.get(ctx.expression());
        Type rhsType = rhs.getType();

        if (rhs instanceof FunctionSymbol) {

        } else {
            DeclarationSymbol declarationSymbol = (DeclarationSymbol) ast.get(ctx);
            Type lhsType = declarationSymbol.getType();
            Type unified = unifyTypes(lhsType, rhsType);
            declarationSymbol.setType(unified);
        }
    }

    /**
     * Result is a BinaryExpressionSymbol
     *
     * @param ctx (MUL | DIV | MOD)
     */
    @Override
    public void exitMultiplicative(@NotNull AquaParser.MultiplicativeContext ctx) {
        Symbol left = ast.get(ctx.expression(0));
        Symbol right = ast.get(ctx.expression(1));

        Type unified = unifyTypes(left.getType(), right.getType());
        ast.get(ctx).setType(unified);
    }

    /**
     * Result is a BinaryExpressionSymbol
     *
     * @param ctx (ADD | SUB)
     */
    @Override
    public void exitAdditive(@NotNull AquaParser.AdditiveContext ctx) {
        Symbol left = ast.get(ctx.expression(0));
        Symbol right = ast.get(ctx.expression(1));

        Type unified = unifyTypes(left.getType(), right.getType());
        ast.get(ctx).setType(unified);
    }

    /**
     * Result is a BinaryExpressionSymbol
     *
     * @param ctx (LE | GE | GT | LT)
     */
    @Override
    public void exitRelational(@NotNull AquaParser.RelationalContext ctx) {
        Symbol left = ast.get(ctx.expression(0));
        Symbol right = ast.get(ctx.expression(1));

        Type unified = unifyTypes(left.getType(), right.getType());
        ast.get(ctx).setType(unified);
    }
}

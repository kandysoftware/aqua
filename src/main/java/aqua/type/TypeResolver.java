package aqua.type;

import aqua.grammar.AquaParser;

/**
 * Resolves types from type annotations.
 */
public class TypeResolver {
    /**
     * Attempts to find type from a given context.
     *
     * @param ctx TypeAnnotationContext
     * @return {@link aqua.type.Type}
     */
    public static Type resolveType(AquaParser.TypeAnnotationContext ctx) {
        TypeResolver tr = new TypeResolver();
        return tr.resolve(ctx);
    }

    public Type resolve(AquaParser.TypeAnnotationContext ctx) {
        return ctx == null ? new UnknownType() : resolve(ctx.type());
    }

    public Type resolve(AquaParser.TypeContext ctx) {
        return resolve(ctx.primitiveType());
    }

    public Type resolve(AquaParser.PrimitiveTypeContext ctx) {
        if (ctx instanceof AquaParser.BooleanTypeContext) return new BoolType();
        if (ctx instanceof AquaParser.DoubleTypeContext) return new FloatType();
        if (ctx instanceof AquaParser.IntegerTypeContext) return new IntType();
        return new UnknownType();
    }
}

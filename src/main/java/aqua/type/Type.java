/**
 * Copyright 2013 Kandy Software Inc.,
 */
package aqua.type;

import java.util.ArrayList;
import java.util.List;

/**
 * Abstract type of all Aqua types.
 */
public abstract class Type implements Cloneable {

    public enum Kind {
        INT, FLOAT, BOOL, STRING, TUPLE, FUNCTION, ACTOR, TYPE, VOID, BOTTOM, UNKNOWN
    }

    private Kind kind;

    public Type(Kind kind) {
        this.kind = kind;
    }

    public String llvmType() {
        return "void";
    }

    public String llvmPointerType() {
        return "void*";
    }

    public String llvmAlignment() {
        return "";
    }

    @Override
    public abstract Type clone();

    public final List<Type> cloneAll(final List<Type> types) {
        final List<Type> copy = new ArrayList<Type>();
        for (final Type t : types) {
            copy.add(t.clone());
        }
        return copy;
    }

    public final Kind getKind() {
        return kind;
    }

    public Boolean isOfKind(final Kind comp) {
        return comp == Kind.BOTTOM ? true : comp == kind;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((kind == null) ? 0 : kind.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Type other = (Type) obj;
        if (kind == Kind.BOTTOM || other.kind == Kind.BOTTOM)
            return false;
        if (kind != other.kind)
            return false;
        return true;
    }
}

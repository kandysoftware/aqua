/**
 * Copyright 2013 Kandy Software Inc.,
 */
package aqua.semantics;

import aqua.type.Type;
import aqua.type.UnknownType;

/**
 * A {@link FreeVariableSymbol} represents an underscore
 */
public class FreeVariableSymbol extends Symbol {

	public FreeVariableSymbol() {
		super(null, null);
	}

	public FreeVariableSymbol(String name) {
		super(name, new UnknownType());
	}

	public FreeVariableSymbol(String name, Type type) {
		super(name, type);
	}

	@Override
	public String toString() {
		return "Underscore(name=" + name + ", type=" + type + ")";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FreeVariableSymbol other = (FreeVariableSymbol) obj;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}
}

/**
 * Copyright 2013 Kandy Software Inc.,
 */
package aqua.semantics;

import aqua.type.Type;

/**
 * A {@link CallSymbol} represents a call expression.
 */
public class CallSymbol extends ExpressionSymbol {

    protected TupleSymbol arguments;

    public CallSymbol(Scope scope, String name, TupleSymbol arguments, Type returnType) {
        super(name, returnType);
        this.scope = scope;
        this.arguments = arguments;
    }

    public TupleSymbol getArguments() {
        return arguments;
    }

    public void setArguments(TupleSymbol arguments) {
        this.arguments = arguments;
    }

    @Override
    public String toString() {
        return name + "(" + arguments + ")";
    }
}

/**
 * Copyright 2013 Kandy Software Inc.,
 */
package aqua.semantics;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * A {@link AbstractScope}.
 */
public abstract class AbstractScope implements Scope {

    private Map<String, Symbol> symbolTable;

    private Scope parentScope;

    /**
     * Creates a root scope.
     */
    public AbstractScope() {
        this.symbolTable = new LinkedHashMap<>();
    }

    /**
     * Creates a new scope within the given scope.
     *
     * @param enclosingScope parent scope
     */
    public AbstractScope(Scope enclosingScope) {
        this();
        this.parentScope = enclosingScope;
    }

    /**
     * Returns parent scope. Null if this is the root scope.
     *
     * @return parent scope
     */
    public Scope getEnclosingScope() {
        return this.parentScope;
    }

    /**
     * Adds a symbol to the scope.
     *
     * @param symbol symbol in scope
     * @return true if symbol by that name already exists in the scope.
     */
    public void define(Symbol symbol) {
        this.symbolTable.put(symbol.getName(), symbol);
    }

    /**
     * Lookup symbol in current scope. If not found look in parent scope.
     *
     * @param name identifier to lookup
     * @return symbol associated with this identifier
     */
    public Symbol resolve(String name) {
        if (name == null) return null;
        Symbol s = this.symbolTable.get(name);
        return s != null ? s : this.parentScope != null ? this.parentScope.resolve(name) : null;
    }

    /**
     * @return symbol table for this scope
     */
    public Map<String, Symbol> getSymbolTable() {
        return symbolTable;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("{");
        for (Map.Entry<String, Symbol> entry : symbolTable.entrySet()) {
            sb.append(entry.getKey());
            sb.append(":");
            sb.append(entry.getValue());
        }
        sb.append("}");
        return sb.toString();
    }
}

/**
 * Copyright 2013 Kandy Software Inc.,
 */
package aqua.semantics;

import aqua.type.Type;

/**
 * A {@link ExpressionSymbol} represents any expression.
 */
public class ExpressionSymbol extends Symbol {

	public ExpressionSymbol() {
		super(null, null);
	}

	public ExpressionSymbol(String name, Type type) {
		super(name, type);
	}

	@Override
	public String toString() {
		return "Expression [type=" + type + "]";
	}
}

/**
 * Copyright 2013 Kandy Software Inc.,
 */
package aqua.semantics;

import aqua.type.Type;

/**
 * A {@link ImmutableSymbol} represents an immutable identifier
 */
public class ImmutableSymbol extends VariableSymbol {

	public ImmutableSymbol() {
		super(null, null);
	}

	public ImmutableSymbol(String name) {
		super(name, null);
	}

	public ImmutableSymbol(String name, Type type) {
		super(name, type);
	}

	@Override
	public String toString() {
		return "Immutable(name=" + name + ", type=" + type + ")";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ImmutableSymbol other = (ImmutableSymbol) obj;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}
}

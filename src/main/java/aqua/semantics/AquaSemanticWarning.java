/**
 * Copyright 2013 Kandy Software Inc.,
 */
package aqua.semantics;

import aqua.compiler.AquaMessage;

/**
 * A {@link AquaSemanticWarning} represents a compile time semantic warning.
 */
public class AquaSemanticWarning extends AquaMessage {

	public AquaSemanticWarning(int line, int position, String description) {
		this.line = line;
		this.position = position;
		this.description = description;
	}
}

/**
 * Copyright 2013 Kandy Software Inc.,
 */
package aqua.semantics;

import java.util.ArrayList;
import java.util.List;

import aqua.type.TupleType;

/**
 * A {@link TupleSymbol} represents a tuple node and it's type.
 */
public class TupleSymbol extends Symbol {

    private List<Symbol> members;

    public TupleSymbol(Scope scope) {
        super("", new TupleType());
        this.members = new ArrayList<>();
        this.scope = scope;
    }

    public TupleSymbol(Scope scope, String name, List<Symbol> members) {
        super(name, new TupleType(extractTypes(members)));
        this.scope = scope;
        this.members = new ArrayList<>(members);
    }

    public List<Symbol> getMembers() {
        return members;
    }

    public void setMembers(List<Symbol> members) {
        this.members = members;
    }

    public void addMember(VariableSymbol var) {
        members.add(var);
    }

    public void resetType() {
        setType(new TupleType(extractTypes(members)));
    }

    @Override
    public String toString() {
        return "Tuple [name=" + name + ", members=" + members + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        TupleSymbol other = (TupleSymbol) obj;
        if (type == null) {
            if (other.type != null)
                return false;
        } else if (!type.equals(other.type))
            return false;
        return true;
    }
}

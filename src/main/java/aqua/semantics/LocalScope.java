package aqua.semantics;

/**
 * Represents global scope.
 */
public class LocalScope extends AbstractScope {

    public LocalScope(Scope enclosingScope) {
        super(enclosingScope);
    }

    @Override
    public String getScopeName() {
        return "local";
    }
}

/**
 * Copyright 2013 Kandy Software Inc.,
 */
package aqua.semantics;

import aqua.type.Type;

/**
 * A {@link aqua.semantics.DeclarationSymbol} represents let, const declarations.
 */
public class DeclarationSymbol extends Symbol {

    protected Symbol rhs;

    protected boolean immutable;

    private DeclarationSymbol() {
        super(null, null);
    }

    public DeclarationSymbol(String name, Type type, Symbol rhs) {
        super(name, type);
        this.rhs = rhs;
    }

    public DeclarationSymbol(String name, Type type, Symbol rhs, boolean immutable) {
        super(name, type);
        this.rhs = rhs;
        this.immutable = immutable;
    }

    public Symbol getRhs() {
        return rhs;
    }

    public void setRhs(Symbol rhs) {
        this.rhs = rhs;
    }

    public boolean isImmutable() {
        return immutable;
    }

    public void setImmutable(boolean immutable) {
        this.immutable = immutable;
    }

    @Override
    public String toString() {
        return String.format("Declaration [%s %s:%s = %s]", (immutable ? "const" : "let"), name, type, rhs);
    }
}

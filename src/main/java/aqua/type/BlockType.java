/**
 * Copyright 2013 Kandy Software Inc.,
 */
package aqua.type;

/**
 * Blocks.
 */
public class BlockType extends Type {

	private Type returnType;

	public BlockType(final Type ret) {
		super(Kind.UNKNOWN);
		returnType = ret;
	}

	@Override
	public String toString() {
		return "(" + returnType + ")";
	}

	/**
	 * @see aqua.type.Type#clone()
	 */
	@Override
	public Type clone() {
		return new BlockType(returnType);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((returnType == null) ? 0 : returnType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		BlockType other = (BlockType) obj;
		if (returnType == null) {
			if (other.returnType != null)
				return false;
		} else if (!returnType.equals(other.returnType))
			return false;
		return true;
	}
}

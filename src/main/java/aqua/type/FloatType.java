/**
 * Copyright 2013 Kandy Software Inc.,
 */
package aqua.type;

/**
 * float
 */
public class FloatType extends NumericType {

    public FloatType() {
        super(Type.Kind.FLOAT);
        this.sizeInBits = 32;
    }

    public FloatType(int sizeInBits) {
        this();
        this.sizeInBits = sizeInBits;
    }

    @Override
    public String llvmType() {
        return String.format("i%s", sizeInBits);
    }

    @Override
    public String llvmPointerType() {
        return String.format("i%s*", sizeInBits);
    }

    @Override
    public String llvmAlignment() {
        return String.format("align 4");
    }

    @Override
    public String toString() {
        return "float";
    }

    /**
     * @see aqua.type.Type#clone()
     */
    @Override
    public Type clone() {
        return new FloatType(sizeInBits);
    }

}

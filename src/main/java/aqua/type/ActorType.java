/**
 * Copyright 2013 Kandy Software Inc.,
 */
package aqua.type;

/**
 * Actor types.
 */
public class ActorType extends Type {

	private Type returnType;

	private TupleType tupleType;

	public ActorType(final Type ret, final TupleType tuple) {
		super(Kind.ACTOR);
		tupleType = tuple;
		returnType = ret;
	}

	@Override
	public String toString() {
		return "(" + tupleType + " -> " + returnType + ")";
	}

	/**
	 * @see aqua.type.Type#clone()
	 */
	@Override
	public Type clone() {
		return new ActorType(returnType, tupleType);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((returnType == null) ? 0 : returnType.hashCode());
		result = prime * result + ((tupleType == null) ? 0 : tupleType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ActorType other = (ActorType) obj;
		if (returnType == null) {
			if (other.returnType != null)
				return false;
		} else if (!returnType.equals(other.returnType))
			return false;
		if (tupleType == null) {
			if (other.tupleType != null)
				return false;
		} else if (!tupleType.equals(other.tupleType))
			return false;
		return true;
	}
}

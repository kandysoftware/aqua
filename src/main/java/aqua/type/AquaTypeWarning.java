/**
 * Copyright (c) 2013 Kandy Software Inc. All rights reserved.
 */
package aqua.type;

import aqua.compiler.AquaMessage;

/**
 * A {@link AquaTypeWarning} represents a warning to be issued for issues
 * discovered during type chcking phase.
 */
public class AquaTypeWarning extends AquaMessage {

	public AquaTypeWarning(int line, int position, String description) {
		this.line = line;
		this.position = position;
		this.description = description;
	}

}

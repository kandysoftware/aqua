/**
 * Copyright 2013 Kandy Software Inc.,
 */
package aqua.semantics;

/**
 * A {@link BinaryExpressionSymbol} represents any expression.
 */
public class BinaryExpressionSymbol extends ExpressionSymbol {

    protected Symbol left;

    protected Symbol right;

    protected String operator;

    public BinaryExpressionSymbol(Symbol left, String operator, Symbol right) {
        super(null, null);
        this.left = left;
        this.right = right;
        this.operator = operator;
    }

    public Symbol getLeft() {
        return left;
    }

    public void setLeft(Symbol left) {
        this.left = left;
    }

    public Symbol getRight() {
        return right;
    }

    public void setRight(Symbol right) {
        this.right = right;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    @Override
    public String toString() {
        return String.format("(left=%s %s right=%s)", left, operator, right);
    }
}

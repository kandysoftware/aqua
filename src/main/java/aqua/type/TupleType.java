/**
 * Copyright 2013 Kandy Software Inc.,
 */
package aqua.type;

import java.util.ArrayList;
import java.util.List;

/**
 * Tuple type (...)
 */
public class TupleType extends Type {

	private List<Type> memberTypes;

	public TupleType() {
		super(Kind.TUPLE);
		this.memberTypes = new ArrayList<>();
	}

	public TupleType(final List<Type> memberTypes) {
		super(Kind.TUPLE);
		this.memberTypes = new ArrayList<>(memberTypes);
	}

	public List<Type> getMemberTypes() {
		return memberTypes;
	}

	public void setMemberTypes(List<Type> memberTypes) {
		this.memberTypes = memberTypes;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("(");
		if (memberTypes != null) {
			int i = 0;
			for (Type type : memberTypes) {
				sb.append(type.toString());
				if (i++ < memberTypes.size()) {
					sb.append(",");
				}
			}
		}
		sb.append(")");
		return sb.toString();
	}

	/**
	 * @see aqua.type.Type#clone()
	 */
	@Override
	public Type clone() {
		return new TupleType(memberTypes);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((memberTypes == null) ? 0 : memberTypes.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TupleType other = (TupleType) obj;

		if (memberTypes.size() == other.memberTypes.size()) {
			int i = 0;
			for (final Type arg : memberTypes) {
				if (!arg.equals(other.memberTypes.get(i++))) {
					return false;
				}
			}
			return true;
		}
		return false;
	}
}

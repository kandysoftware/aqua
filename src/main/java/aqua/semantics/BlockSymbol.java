/**
 * Copyright 2013 Kandy Software Inc.,
 */
package aqua.semantics;

import aqua.type.Type;

/**
 * A {@link BlockSymbol} represents a block with a local scope.
 */
public class BlockSymbol extends Symbol {

    public BlockSymbol(Scope scope, Type type) {
        super(null, type);
        this.scope = scope;
    }

    @Override
    public String toString() {
        return "Block [type=" + type + ", scope=" + scope + "]";
    }
}

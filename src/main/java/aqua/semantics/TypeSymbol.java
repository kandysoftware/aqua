/**
 * Copyright 2013 Kandy Software Inc.,
 */
package aqua.semantics;

import java.util.ArrayList;
import java.util.List;

import aqua.type.TypeType;

/**
 * A {@link TypeSymbol} represents a Type node
 */
public class TypeSymbol extends Symbol {

	private List<Symbol> properties;

	private List<FunctionSymbol> methods;

	public TypeSymbol(String name) {
		super(name, new TypeType());
		properties = new ArrayList<>();
		methods = new ArrayList<>();
	}

	public TypeSymbol(String name, List<Symbol> properties) {
		super(name, new TypeType());
		this.properties = properties == null ? new ArrayList<Symbol>() : new ArrayList<>(properties);
		this.methods = new ArrayList<>();
	}

	public TypeSymbol(String name, List<Symbol> properties, List<FunctionSymbol> methods) {
		super(name, new TypeType(extractTypes(methods)));
		this.properties = properties == null ? new ArrayList<Symbol>() : new ArrayList<>(properties);
		this.methods = methods == null ? new ArrayList<FunctionSymbol>() : new ArrayList<>(methods);
	}

	@Override
	public String toString() {
		return "Type [name=" + name + ", properties=" + properties + ", methods=" + methods + "]";
	}

	public List<Symbol> getProperties() {
		return properties;
	}

	public void setProperties(List<Symbol> properties) {
		this.properties = properties;
	}

	public List<FunctionSymbol> getMethods() {
		return methods;
	}

	public void setMethods(List<FunctionSymbol> methods) {
		this.methods = methods;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((methods == null) ? 0 : methods.hashCode());
		result = prime * result + ((properties == null) ? 0 : properties.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TypeSymbol other = (TypeSymbol) obj;
		if (methods == null) {
			if (other.methods != null)
				return false;
		} else if (!methods.equals(other.methods))
			return false;
		if (properties == null) {
			if (other.properties != null)
				return false;
		} else if (!properties.equals(other.properties))
			return false;
		return true;
	}
}

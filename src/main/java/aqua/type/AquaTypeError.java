/**
 * Copyright (c) 2013 Kandy Software Inc. All rights reserved.
 */
package aqua.type;

import aqua.compiler.AquaMessage;

/**
 * A {@link AquaTypeError} represents an error discovered in type checking
 * phase.
 */
public class AquaTypeError extends AquaMessage {

	public AquaTypeError(int line, int position, String description) {
		this.line = line;
		this.position = position;
		this.description = description;
	}

}
